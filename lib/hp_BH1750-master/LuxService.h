//LuxService::sensor.hasValue()
// Created by cedu on 8/8/20.
//
#include <Arduino.h>
#include <hp_BH1750.h>

#ifndef GROWIOT_LUXSERVICE_H
#define GROWIOT_LUXSERVICE_H


class LuxService {
public:
    static String readPressureData();
    static void initialize();
private:
    static hp_BH1750 sensor;
};


#endif //GROWIOT_LUXSERVICE_H
