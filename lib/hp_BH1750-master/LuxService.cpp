//
// Created by cedu on 8/8/20.
//

#include "LuxService.h"

void LuxService::initialize() {
    bool avail = LuxService::sensor.begin(BH1750_TO_GROUND);   // will be false no sensor found
    LuxService::sensor.start();
}

String LuxService::readPressureData() {
    int counter = 0;
    while(!LuxService::sensor.hasValue() && counter < 50) {    // non blocking reading
        delay(5);
        counter++;
    }

    if(LuxService::sensor.hasValue()){
        float lux = LuxService::sensor.getLux();
        LuxService::sensor.start();
        return (String) "{ \"Lux\": " + lux + " }";
    }
    return (String) "{ \"Lux\": " + -999 + " }";
}

hp_BH1750 LuxService::sensor = hp_BH1750();
