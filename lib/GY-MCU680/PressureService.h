//LuxService::sensor.hasValue()
// Created by cedu on 8/8/20.
//
#include <Arduino.h>
#include <Adafruit_BME680.h>

#ifndef GROWIOT_PRESSURESERVICE_H
#define GROWIOT_PRESSURESERVICE_H


class PressureService {
public:
    static String readPressureData();
    static void initialize();
private:
    static Adafruit_BME680 sensor;
};


#endif //GROWIOT_PRESSURESERVICE_H
