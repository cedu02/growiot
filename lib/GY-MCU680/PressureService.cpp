//
// Created by cedu on 8/8/20.
//

#include "PressureService.h"
#define SEALEVELPRESSURE_HPA (1013.25)

String PressureService::readPressureData() {
    int counter = 0;

    while (!PressureService::sensor.performReading() && counter < 50) {
        delay(5);
        counter++;
    }

    return (String) "{ \"Pressure\": " + PressureService::sensor.pressure / 100.0 +
           ", \"Temp\": " + PressureService::sensor.temperature +
           ", \"Humidity\": " + PressureService::sensor.humidity +
           ", \"Gas\": " + PressureService::sensor.gas_resistance +
           ", \"Approx. Altitude\": " + PressureService::sensor.readAltitude(SEALEVELPRESSURE_HPA) + " }";
}

void PressureService::initialize() {
    // start sensor.
    if(!PressureService::sensor.begin(0x76)){
        Serial.println("Failed to start sensor! Please check your wiring.");
    }
}

Adafruit_BME680 PressureService::sensor =  Adafruit_BME680();
