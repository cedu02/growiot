//getWaterLevel
// Created by cedu on 3/16/20.
//

#include "GroundHumiditySensor.h"

int GroundHumiditySensor::getGroundHumidity(int sensor) {
    long value = analogRead(sensor);
    value = constrain(value,400,1023);	//Keep the ranges!
    value = map(value,400,1023,100,0);
    return value;
}
