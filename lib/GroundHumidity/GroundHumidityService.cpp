//
// Created by cedu on 8/5/20.
//

#include "GroundHumidityService.h"

String GroundHumidityService::readGroundHumidityData() {
    long humiditySensor0 = GroundHumiditySensor::getGroundHumidity(A0);
    long humiditySensor1 = GroundHumiditySensor::getGroundHumidity(A1);
    long humiditySensor2 = GroundHumiditySensor::getGroundHumidity(A2);
    long humiditySensor3 = GroundHumiditySensor::getGroundHumidity(A3);

    return (String) "{ \"Sensor0\": " + humiditySensor0 +
           ", \"Sensor1\": " + humiditySensor1 +
           ", \"Sensor2\": " + humiditySensor2 +
           ", \"Sensor3\": " + humiditySensor3 +" }";
}