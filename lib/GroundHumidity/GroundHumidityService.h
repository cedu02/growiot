//
// Created by cedu on 8/5/20.
//
#include <Arduino.h>
#include "GroundHumiditySensor.h"

#ifndef GROWIOT_GROUNDHUMIDITYSERVICE_H
#define GROWIOT_GROUNDHUMIDITYSERVICE_H


class GroundHumidityService {
public:
    static String readGroundHumidityData();
};


#endif //GROWIOT_GROUNDHUMIDITYSERVICE_H
