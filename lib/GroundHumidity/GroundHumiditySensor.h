//
// Created by cedu on 3/16/20.
//
#include <Arduino.h>

#ifndef STRAWBERRZ_GROUNDHUMIDITYSENSOR_H
#define STRAWBERRZ_GROUNDHUMIDITYSENSOR_H

class GroundHumiditySensor {
public: static int getGroundHumidity(int sensor);
};

#endif //STRAWBERRZ_GROUNDHUMIDITYSENSOR_H
