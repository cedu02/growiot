#include "DHT11Sensor.h"

/**
 * Initialize sensors.
 */
DHT DHT11Sensor::sensorFrontLeft = DHT(2, DHT11);
DHT DHT11Sensor::sensorFrontRight = DHT(3, DHT11);
DHT DHT11Sensor::sensorBackLeft = DHT(4, DHT11);
DHT DHT11Sensor::sensorBackRight = DHT(5, DHT11);

/**
 * Build json string with all temps and humidity by sensor.
 * @return
 */
String DHT11Sensor::readDataForService() {

    float tempSensorFrontLeft = DHT11Sensor::sensorFrontLeft.readTemperature();
    float humidityFrontLeft = DHT11Sensor::sensorFrontLeft.readHumidity();
    String frontLeft = R"({"FrontLeft": {"Temp": )" + String(tempSensorFrontLeft, 2) +
            ", \"Humidity\": " + String(humidityFrontLeft, 2) + "},";

    float tempSensorFrontRight = DHT11Sensor::sensorFrontRight.readTemperature();
    float humidityFrontRight = DHT11Sensor::sensorFrontRight.readHumidity();
    String frontRight = R"("FrontRight": {"Temp": )" + String(tempSensorFrontRight, 2) +
                       ", \"Humidity\": " + String(humidityFrontRight, 2) + "},";


    float tempSensorBackLeft = DHT11Sensor::sensorBackLeft.readTemperature();
    float humidityBackLeft = DHT11Sensor::sensorBackLeft.readHumidity();

    String backLeft = R"("BackLeft": {"Temp": )" + String(tempSensorBackLeft, 2) +
                        ", \"Humidity\": " + String(humidityBackLeft, 2) + "},";

    float tempSensorBackRight = DHT11Sensor::sensorBackRight.readTemperature();
    float humidityBackRight = DHT11Sensor::sensorBackRight.readHumidity();
    String backRight = R"("BackRight": {"Temp": )" + String(tempSensorBackRight, 2) +
                        ", \"Humidity\": " + String(humidityBackRight, 2) + "}}";

    return frontLeft + frontRight + backLeft + backRight;
}

/**
 * Start up sensors.
 */
void DHT11Sensor::initialize() {
    DHT11Sensor::sensorFrontLeft.begin();
    DHT11Sensor::sensorFrontRight.begin();
    DHT11Sensor::sensorBackLeft.begin();
    DHT11Sensor::sensorBackRight.begin();
}