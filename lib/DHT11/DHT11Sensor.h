/**
 * Temperatur and Humidity Sensor DHT11.
 */
#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>


#ifndef STRAWBERRZ_TEMPERATURESENSOR_H
#define STRAWBERRZ_TEMPERATURESENSOR_H


class DHT11Sensor {
public:
    static String readDataForService();
    static void initialize();

private:
    static DHT sensorFrontLeft;
    static DHT sensorFrontRight;
    static DHT sensorBackLeft;
    static DHT sensorBackRight;
};
#endif //STRAWBERRZ_TEMPERATURESENSOR_H
