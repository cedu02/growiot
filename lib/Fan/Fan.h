//
// Created by cedu on 4/5/20.
//

#ifndef STRAWBERRYSENSORS_FAN_H
#define STRAWBERRYSENSORS_FAN_H


class Fan {
public:
    static void startFans();
    static void stopFans();
    static void registerFans();
    static bool isFanRunning();
};
#endif //STRAWBERRYSENSORS_FAN_H
