//
// Created by cedu on 4/5/20.
//

#include <HID.h>
#include "Fan.h"

#define in1 8
#define in2 9

#define in3 10
#define in4 11


void Fan::startFans() {
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);

    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);
}

void Fan::stopFans() {
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);

    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW);
}

void Fan::registerFans() {
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);
    stopFans();
}

bool Fan::isFanRunning() {
    return digitalRead(in1) == HIGH && digitalRead(in3) == HIGH;
}
