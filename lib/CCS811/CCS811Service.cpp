//
// Created by cedu on 8/7/20.
//

#include "CCS811Service.h"

String CCS811Service::readAirQualityData() {
    int counter = 0;

    while(!CCS811Service::sensor.available() && counter < 50){
        delay(5);
        counter++;
    }

    if(!CCS811Service::sensor.readData()){
        return (String) "{ \"Co2\": " + CCS811Service::sensor.geteCO2() +
               ", \"TVOC\": " + CCS811Service::sensor.getTVOC() + " }";
    }else{
        return (String) "{ \"Co2\": " + 0 +
               ", \"TVOC\": " + 0 + " }";
    }
}

void CCS811Service::initialize() {
    // start sensor.
    if(!CCS811Service::sensor.begin()){
        Serial.println("Failed to start sensor! Please check your wiring.");
        // while(1);
    }
    // wait for sensor to be ready
    while(!CCS811Service::sensor.available());
}

Adafruit_CCS811 CCS811Service::sensor =  Adafruit_CCS811();