//ccs.begin()
// Created by cedu on 8/7/20.
//
#include <Arduino.h>
#include <Adafruit_CCS811.h>


#ifndef GROWIOT_CCS811SERVICE_H
#define GROWIOT_CCS811SERVICE_H


class CCS811Service {
public:
    static String readAirQualityData();
    static void initialize();
private:
    static Adafruit_CCS811 sensor;
};


#endif //GROWIOT_CCS811SERVICE_H
