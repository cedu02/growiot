//
// Created by cedu on 8/5/20.
//
#include <Arduino.h>

#ifndef GROWIOT_SHT10_H
#define GROWIOT_SHT10_H


class SHT10 {
public:
    SHT10(int dataPin, int clockPin);
    float readHumidity();
    float readTemperatureC();
    float readTemperatureF();
private:
    int _dataPin;
    int _clockPin;
    int _numBits;
    float readTemperatureRaw();
    int shiftIn(int _dataPin, int _clockPin, int _numBits);
    void sendCommandSHT(int _command, int _dataPin, int _clockPin);
    void waitForResultSHT(int _dataPin);
    int getData16SHT(int _dataPin, int _clockPin);
    void skipCrcSHT(int _dataPin, int _clockPin);
};


#endif //GROWIOT_SHT10_H
