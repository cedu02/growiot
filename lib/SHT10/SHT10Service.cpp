//
// Created by cedu on 8/5/20.
//

#include "SHT10Service.h"

String SHT10Service::readTempAndHumidityData() {
    return (String) "{ \"Temp\": " + SHT10Service::sensor.readTemperatureC() +
    ", \"Humidity\": " + SHT10Service::sensor.readHumidity() + " }";
}

SHT10 SHT10Service::sensor = SHT10(6, 7);