//
// Created by cedu on 8/5/20.
//

#include <Arduino.h>
#include "SHT10.h"

#ifndef GROWIOT_SHT10SERVICE_H
#define GROWIOT_SHT10SERVICE_H


class SHT10Service {
public:
    static String readTempAndHumidityData();

private:
    static SHT10 sensor;
};
#endif //GROWIOT_SHT10SERVICE_H
