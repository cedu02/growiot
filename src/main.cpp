#include <Arduino.h>
#include <SerialCommunication.h>
#include <DHT11Sensor.h>
#include <GroundHumidityService.h>
#include <CCS811Service.h>
#include "../lib/Fan/Fan.h"
#include "../lib/SHT10/SHT10.h"
#include "../lib/SHT10/SHT10Service.h"
#include "../lib/GY-MCU680/PressureService.h"

void setup() {
    Serial.begin(9600);
    PressureService::initialize();
    CCS811Service::initialize();
    DHT11Sensor::initialize();

}

String getSuccessString();
String failedTaskString(const String& message);


void loop() {
    TaskEnum task = SerialCommunication::getTask();
    switch (task) {
        case TaskEnum::StartFan:
            Fan::startFans();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::StopFan:
            Fan::stopFans();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::IsFanRunning:
            SerialCommunication::sendResult((String)"{\"Running\": " + Fan::isFanRunning() + " }");
            break;
        case TaskEnum::ReadHumidityPlants:
            SerialCommunication::sendResult(GroundHumidityService::readGroundHumidityData());
            break;
        case TaskEnum::ReadTemperatureHumidity:
            SerialCommunication::sendResult(DHT11Sensor::readDataForService());
            break;
        case TaskEnum::ReadTemperatureHumidityMiddle:
            SerialCommunication::sendResult(SHT10Service::readTempAndHumidityData());
            break;
        case TaskEnum::ReadAirQuality:
            SerialCommunication::sendResult(CCS811Service::readAirQualityData());
            break;
        case TaskEnum::ReadPressure:
            SerialCommunication::sendResult(PressureService::readPressureData());
            break;
        case TaskEnum::ReadLux:
            SerialCommunication::sendResult(failedTaskString("Not working"));
            break;
        case TaskEnum::NoOpenTask:
            break;
        default:
            SerialCommunication::sendResult( failedTaskString("Not implemented"));
    }
}


String getSuccessString() {
    return (String) "{\"Success\": " + true + " }";
}

String failedTaskString(const String& message) {
    return (String) "{\"Success\": " + false + " Message: " + message + " }";
}